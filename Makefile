GL_GIT=git@gitlab.com:rd--/tctl.git
GL_HTTP=https://gitlab.com/rd--/tctl.git

all:
	echo "tctl"

mk-cmd:
	echo "tctl - NIL"

clean:
	rm -Rf dist dist-newstyle *~
	(cd nexus-ui/hs ; make clean)

push-gl:
	git push $(GL_GIT)

pull-gl:
	git pull $(GL_HTTP)

push-tags:
	git push $(GL_GIT) --tags

update-rd:
	ssh rd@rohandrape.net "(cd sw/tctl; git pull $(GL_HTTP))"

push-all:
	make push-gl update-rd
