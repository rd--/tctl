-- | List Ui (Tk)
--
-- Note: There are multiple difficulties with using optionmenu in place of listbox.
--
-- Deprecated: see hsc3-data: "Sound.Sc3.Data.Ui.Html"
module Sound.Sc3.Ctl.List.Tk where

import Control.Concurrent {- base -}

import qualified HTk.Toplevel.HTk as HTk {- uni-htk -}

-- | Make Font value from family name and size.
ui_font :: String -> Int -> HTk.Font
ui_font nm sz = HTk.toFont ((read nm)::HTk.FontFamily,sz)

-- | USR action (STATE,IX) -> IO ST
type ACT st = ((st,(Int,Int)) -> IO st)

-- | Get singular index of list selection.
ui_list_get_index :: HTk.ListBox t -> IO (Maybe Int)
ui_list_get_index l = do
  r <- HTk.getSelection l
  case r of
    Just [i] -> return (Just i)
    _ -> return Nothing -- 0 -- error "ui_get_index?"

-- | Make k-th list-box.
ui_list_elem :: HTk.Container c => c -> Int -> MVar st -> ACT st -> (Int,(Int,[String])) -> IO ()
ui_list_elem w sz_h m_st act (k,(sz_w,lst)) = do
  let k_act m_x = case m_x of
                    Just x -> modifyMVar_ m_st (\st -> act (st,(k,x)))
                    Nothing -> return ()
      lst_sz = (HTk.pp (fromIntegral sz_w),HTk.pp (fromIntegral sz_h))
  l <- HTk.newListBox w [HTk.font (ui_font "courier" 12)
                        ,HTk.borderwidth (HTk.pp 0)
                        ,HTk.value lst
                        ,HTk.size lst_sz] :: IO (HTk.ListBox String)
  HTk.grid l [HTk.GridPos (k,0)]
  (b, _) <- HTk.bindSimple l (HTk.ButtonPress (Just 1))
  (r, _) <- HTk.bindSimple l (HTk.KeyPress Nothing)
  _ <- HTk.spawnEvent (HTk.forever (HTk.choose [b,r] >> HTk.always (ui_list_get_index l >>= k_act)))
  return ()

-- | Make list-box for each /lst/ and call /act/ with (st,(lst-ix,ent-ix)) on selection.
--   Sizes are given in CHAR, sz_h is the number of rows, each lst is preceded by a column-width.
--
-- > lst_seq = let lst = map show [0 .. 200] in [(8,lst),(12,lst),(24,lst),(4,lst)]
-- > ui_list (20,True) lst_seq 0 (\(st,x) -> print (st,x) >> return (st + 1))
--
-- > ui_list (40,True) [(80,map (\c -> take 60 (cycle [c,' '])) "ABCD")] () (\(_,x) -> print x)
ui_list :: (Int,Bool) -> [(Int,[String])] -> st -> ACT st -> IO ()
ui_list (sz_h,fix) lst_seq st act = do
  m_st <- newMVar st
  let char_w = 7.20 -- estimate for char-width to pixel multiplier
      char_h = char_w * 2.15 -- estimate for char-height
      w_sz = (HTk.pp (sum (map (fromIntegral . fst) lst_seq) * char_w)
             ,HTk.pp (fromIntegral sz_h * char_h))
  let w_opt = if fix then [HTk.minSize w_sz,HTk.maxSize w_sz] else []
  _t <- HTk.initHTk [HTk.withdrawMainWin]
  w <- HTk.createToplevel w_opt -- NOTE: required to make fixed size windows reliable (?)
  mapM_ (ui_list_elem w sz_h m_st act) (zip [0..] lst_seq)
  HTk.pack w [HTk.Fill HTk.Both,HTk.Expand HTk.On]
  (e, _) <- HTk.bindSimple w HTk.Destroy
  HTk.sync e
  HTk.cleanupWish

-- | Variant that calculates width based on text size, /k/ is a CHAR spacing.
--
-- > lst = ["A B C","D E F","G H I","J K L","M N O P Q R"]
-- > ui_list_w 2 (10,True) [lst,take 3 lst] 0 (\(st,x) -> print (st,x) >> return (st + 1))
ui_list_w :: Int -> (Int,Bool) -> [[String]] -> st -> ACT st -> IO ()
ui_list_w k opt txt = ui_list opt (zip (map ((+ k) . maximum . map length) txt) txt)
