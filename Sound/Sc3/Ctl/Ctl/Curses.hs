module Sound.Sc3.Ctl.Ctl.Curses where

import qualified Data.ByteString.Lazy as ByteString {- bytestring -}
import qualified Data.IntMap as IntMap {- containers -}
import qualified UI.HSCurses.Curses as Curses {- hscurses -}

import qualified Sound.Osc.Fd as Osc {- hosc -}
import qualified Sound.Sc3.Fd as Sc3 {- hsc3 -}

data Ctl = Ctl {name :: String
               ,value :: Int}

ctl :: Ctl
ctl = Ctl {name = "        "
          ,value = 0}

extract :: Osc.Message -> Maybe (Int,Int)
extract m =
    case m of
      Osc.Message "/midi" [Osc.Int32 _,Osc.Blob d] ->
          case ByteString.unpack d of
            [0xb0,c,x] -> Just (fromIntegral c,fromIntegral x)
            _ -> Nothing
      _ -> Nothing

set_ctl :: Osc.Transport t => t -> (Int,Int) -> IO ()
set_ctl fd (i,x) =
    let x_ = (fromIntegral x / (128.0 / 2.0)) - 1.0
    in Osc.sendMessage fd (Sc3.c_set1 i x_)

c_init :: IO Curses.Window
c_init = do
  w <- Curses.initScr
  Curses.keypad w True
  Curses.nl False
  Curses.cBreak True
  Curses.echo False
  _ <- Curses.leaveOk True
  return w

pad :: Bool -> Int -> String -> String
pad d n xs =
    let m = length xs
    in case compare m n of
         EQ -> xs
         GT -> take n xs
         LT -> let s = replicate (n - m) ' '
               in if d then xs ++ s else s ++ xs

padl :: Int -> String -> String
padl = pad False

c_draw :: Curses.Window -> IntMap.IntMap Ctl -> (Int,Int) -> IO ()
c_draw w m (i,x) = do
  let fw = 8
      n = 8
      c = (i `rem` n) * fw
      r = (i `quot` n) * 2
      a = Curses.attr0  -- if even i then Curses.attr0 else Curses.setReverse Curses.attr0 True
  Curses.attrOn a
  Curses.mvWAddStr w r c (padl fw (name (IntMap.findWithDefault ctl i m)))
  Curses.mvWAddStr w (r + 1) c ("  " ++ padl 3 (show x) ++ "   ")
  Curses.attrOff a
  Curses.refresh

c_end :: Curses.Window -> IO ()
c_end _ = do
  _ <- Curses.getch
  Curses.endWin

run_ctl :: [(Int,Ctl)] -> IO ()
run_ctl ics = do
  m <- Osc.openOscSocket (Osc.Udp, "127.0.0.1", 57150) -- midi.osc
  s <- Osc.openOscSocket (Osc.Tcp, "127.0.0.1", 57110) -- scsynth
  w <- c_init
  let u' = IntMap.fromList ics
  Osc.sendMessage m (Osc.Message "/receive" [Osc.Int32 0xffff])
  Osc.sendMessage s (Sc3.c_getn [(0,96)])
  _:_:xs <- Osc.waitDatum s "/c_setn"
  let to_int (Osc.Float x) = floor (x * 127.0)
      to_int _ = 0
  mapM_ (c_draw w u') (zip [0..95] (map to_int xs))
  Sc3.repeatM_
       (do r:_ <- Osc.recvMessages m
           let ix = extract r
               act a = set_ctl s a >> c_draw w u' a
           maybe (return ()) act ix)
  c_end w
