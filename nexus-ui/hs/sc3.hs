-- nuxus-ui is re-written...  ie. http://nexus-js.github.io/ui/

import qualified Network.CGI as C {- cgi -}

import qualified Sound.OSC as O {- hosc -}
import qualified Sound.SC3 as S {- hsc3 -}

import qualified WWW.Minus.CGI as W {- www-minus -}

log_error :: (O.MonadIO m,C.MonadCGI m) => [String] -> m ()
log_error txt = C.liftIO (appendFile "/tmp/nexus-ui-sc3.log" (unlines txt))

send_ctl :: (O.MonadIO m,C.MonadCGI m) => String -> String -> m ()
send_ctl k x = C.liftIO (S.withSC3 (O.sendMessage (S.c_set1 (read k) (read x))))

dispatch_post :: (O.MonadIO m,C.MonadCGI m) => W.Parameters -> m ()
dispatch_post _ = do
  osc_nm <- C.getInput "oscName"
  osc_dat <- C.getInput "data"
  case (osc_nm,osc_dat) of
    (Just ('/':k),Just x) -> send_ctl k x
    _ -> log_error ["?",show osc_nm,show osc_dat]
  return ()

dispatch :: (O.MonadIO m,C.MonadCGI m) => () -> W.Dispatch m
dispatch () (m,q) =
  case m of
    "POST" -> dispatch_post (m,q)
    _ -> log_error ["non-POST?",m] >> return ()

main :: IO ()
main = do
  C.runCGI (C.handleErrors (do m <- C.requestMethod
                               q <- C.queryString
                               dispatch () (m,W.cgi_parse_query q)
                               C.output "NIL"))
{-

import Sound.SC3 {- hsc3 -}

kin = in' 1 KR
o = sinOsc AR (kin 0) 0
audition (out 0 (pan2 o (kin 2) (kin 1)))

f = kin 0 * 600 + 400
a = kin 1 * 0.1
audition (out 0 (sinOsc AR f 0 * a))

o = sinOsc AR (midiCPS 65.00) 0.00 * dbAmp (-12.00)
audition (out 0.00 o)

let k n l r = linLin (in' 1 KR n) 0 100 l r
audition (pan2 (sinOsc AR (k 0 20 2000) 0) (k 2 (-1) 1) (k 1 0 0.1))

-}
