{-
midi-osc -p 57150
scsynth -u 57110
-}

import Sound.SC3 {- hsc3 -}
import qualified Sound.SC3.Ctl.Ctl.Curses as C {- tctl -}

main :: IO ()
main = do
  let k_in i l r = linLin (lagIn 1 i 0.1) (-1) 1 l r
      f = k_in 0 600 800
      a = k_in 1 0 0.1
      p = k_in 2 (-1) 1
  withSC3 reset
  audition (out 0 (pan2 (sinOsc AR f 0) p a))
  C.run_ctl [(0,C.ctl {C.name = "  ofreq  "})
            ,(1,C.ctl {C.name = "  oampl  "})
            ,(2,C.ctl {C.name = "  osloc  "})]
